import express from "express";
import routes from "./routes/routes"; // Use the routes defined in the entryRoutes file //
import cors from "cors";
import mysql2 from "mysql2";
import dotenv from "dotenv";

dotenv.config();

const app = express();
const PORT = process.env.PORT || 4000;

app.use(express.json());
app.use(cors());
app.use(routes);

// to connect locally to database - comment out the socketPath connection in the db connection options. App will then connect to the db via localhost

const db = mysql2.createConnection({
  socketPath: process.env.DATABASE_SOCKET,
  host: process.env.DATABASE_HOST,
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_NAME,
});
if (process.env.DATABASE_SOCKET) {
  db.socketPath = process.env.DATABASE_SOCKET;
} else {
  db.host = process.env.DATABASE_HOST;
}

db.connect((err) => {
  if (err) throw err;
  console.log("Connected to database");
});
global.db = db;

app.listen(PORT, () => {
  console.log(`Server started at http://localhost:${PORT}`);
});
