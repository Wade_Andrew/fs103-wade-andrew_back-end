import bcrypt from "bcrypt";

// GET all messages //
const getAllMessages = (req, res) => {
  const sqlGet = "SELECT * FROM messages ORDER BY id DESC";

  db.query(sqlGet, (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(result);
    }
  });
};

// POST new message //
const sendMessage = (req, res) => {
  const { name, email, phoneNumber, message } = req.body;
  const sqlPost = "INSERT INTO messages (name, email, phoneNumber, message) VALUES (?,?,?,?)";

  db.query(sqlPost, [name, email, phoneNumber, message], (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(result);
      console.log(result);
    }
  });
};

// DELETE message from database //
const deleteMessage = (req, res) => {
  const id = req.params.id;
  const sqlDelete = "DELETE FROM messages WHERE id = ?";

  db.query(sqlDelete, id, (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.status(204).send(result);
      console.log(result);
    }
  });
};

// GET data for one message //
const getOneMessage = (req, res) => {
  const id = req.params.id;
  const sqlGet = "SELECT * FROM messages WHERE id = ?";
  db.query(sqlGet, id, (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(result);
    }
  });
};

// PUT (add) note to one message //
const addNoteToMessage = (req, res) => {
  const id = req.params.id;
  const notes = req.body.notes;
  const sqlUpdate = "UPDATE messages SET notes = ? WHERE id = ?";
  db.query(sqlUpdate, [notes, id], (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(result);
    }
  });
};

// GET all résumé items - Work //
const getAllWorkItems = (req, res) => {
  const sqlGet = "SELECT * FROM resume_page_work";

  db.query(sqlGet, (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(result);
    }
  });
};

// GET all résumé items - School //
const getAllSchoolItems = (req, res) => {
  const sqlGet = "SELECT * FROM resume_page_school";

  db.query(sqlGet, (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(result);
    }
  });
};

// GET all portfolio items //
const getAllPortfolioItems = (req, res) => {
  const sqlGet = "SELECT * FROM portfolio_page";

  db.query(sqlGet, (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(result);
    }
  });
};

//POST to database to register user//
const userRegister = async (req, res) => {
  const username = req.body.username;
  const password = await bcrypt.hash(req.body.password, 10); // The number 10 refers to the "salt rounds", or complexity of the hashing algorithm //
  const sqlInsert = "INSERT INTO registered_users (username, password) VALUES (?,?)";

  db.query(sqlInsert, [username, password], (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(result);
      console.log(result);
    }
  });
};

//POST to database to check for matching usernames and passwords for login//
const userLogin = (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  const sqlVerify = "SELECT * FROM registered_users WHERE username = ?";

  db.query(sqlVerify, username, (err, result) => {
    if (err) {
      res.send({ error: err });
    }
    if (result.length > 0) {
      const hashedPassword = result[0].password;
      bcrypt.compare(password, hashedPassword, (error, response) => {
        if (response) {
          res.send(result);
        } else {
          res.send({ message: "Incorrect username or password" });
        }
      });
    } else {
      res.send({ message: "User does not exist" });
    }
  });
};

export {
  getAllMessages,
  sendMessage,
  deleteMessage,
  userRegister,
  userLogin,
  getAllWorkItems,
  getAllSchoolItems,
  getAllPortfolioItems,
  getOneMessage,
  addNoteToMessage,
};
