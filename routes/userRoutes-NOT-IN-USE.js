import express from "express";
import argon2 from "argon2";
import jwt from "express-jwt";
import * as db from "../controllers/controller";
import { v4 as uuidv4 } from "uuid";

const router = express.Router();

const emailRegex =
  /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;
const validateEmail = (email) => {
  let validEmail = emailRegex.test(email);
  if (!validEmail) return false;
  if (email.length > 254) return false;
  else return true;
};

const validateUsers = (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  const invalid = [];
  if (req.body.name == null) {
    invalid.push("Please enter your name");
  }
  if (req.body.password == null) {
    invalid.push("Please enter a password");
  }
  if (password.length < 8) {
    invalid.push("Password must be at least 8 characters");
  }
  if (req.body.email == null || !validateEmail(email)) {
    invalid.push("Please enter a valid email address");
  }
  if (invalid.length > 0) {
    return res.status(400).json({ invalid });
  }
  next();
};

router.use(jwt({ secret: process.env.JWT_SECRET }));

router.get("/", async (req, res) => {
  res.send(await db.getAllEntries());
});

export default router;
