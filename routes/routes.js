import express from "express";
import {
  getAllMessages,
  sendMessage,
  deleteMessage,
  userRegister,
  userLogin,
  getAllWorkItems,
  getAllSchoolItems,
  getAllPortfolioItems,
  getOneMessage,
  addNoteToMessage,
} from "../controllers/controller";

const router = express.Router();

router.post("/contact_form/messages", sendMessage);
router.get("/messages/get", getAllMessages);
router.get("/resume_page_work/get", getAllWorkItems);
router.get("/messages/get/:id", getOneMessage);
router.put("/messages/notes/:id", addNoteToMessage);
router.get("/resume_page_school/get", getAllSchoolItems);
router.get("/portfolio_page/get", getAllPortfolioItems);
router.delete("/deleteMessage/:id", deleteMessage);
router.post("/register", userRegister);
router.post("/login", userLogin);

export default router;
