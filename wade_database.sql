-- MySQL dump 10.13  Distrib 8.0.14, for macos10.14 (x86_64)
--
-- Host: localhost    Database: wade_database
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phoneNumber` varchar(25) NOT NULL,
  `message` text NOT NULL,
  `notes` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (9,'Dave Smith','davesmith@gmail.com','604-877-9874','Hello Wade. This is another test message.','Test note about Dave Smith'),(25,'Andy Anderson','andy@gmail.com','604-884-9874','Message from Andy Anderson.','Test note about Andy'),(27,'Dave Waters','dave@dave.com','778-993-3345','Hi Wade. This is a test message from Dave Waters.','Dave is a great guy.'),(28,'John Wayne','john@johnwayne.com','333-555-2215','Hello from John Wayne.','He\'s been dead for years'),(29,'Steven Williams','stevewilliams@gmail.com','604-877-9021','Test message for Wade from Steve.','Meeting with Steve Wednesday 9am'),(30,'David Rockefeller','david_r@gmail.com','416-998-4482','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.','David likes writing long emails');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portfolio_page`
--

DROP TABLE IF EXISTS `portfolio_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `portfolio_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(65) NOT NULL,
  `alt` varchar(125) NOT NULL,
  `descriptionTop` varchar(255) NOT NULL,
  `descriptionBottom` varchar(255) NOT NULL,
  `link` varchar(65) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolio_page`
--

LOCK TABLES `portfolio_page` WRITE;
/*!40000 ALTER TABLE `portfolio_page` DISABLE KEYS */;
INSERT INTO `portfolio_page` VALUES (1,'./images/website_1.JPG','A woman hiking in the mountains','I took this photo of my partner Naomi hiking in British Columbia in 2019. Living in BC, my summers are dedicated to hiking, camping and outdoor photography.','I created this web page for a fictional British Columbia tourism association.',NULL),(2,'./images/website_2.JPG','A man standing in the water holding a shovel','This is the home page for my personal photography website. I have been an enthusiastic photographer for many years, documenting my hiking trips and travel overseas.','Big, eye-catching images feature prominently on this website.',NULL),(3,'./images/website_3.JPG','A couple beside a lake, a boat in the water, the Taj Mahal','I created this web page for a student project for the York University Web Development program. The assignment was to create an interactive photo gallery.','It was my first ever use of Javascript, which I found very exciting!',NULL);
/*!40000 ALTER TABLE `portfolio_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registered_users`
--

DROP TABLE IF EXISTS `registered_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `registered_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `password_UNIQUE` (`password`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registered_users`
--

LOCK TABLES `registered_users` WRITE;
/*!40000 ALTER TABLE `registered_users` DISABLE KEYS */;
INSERT INTO `registered_users` VALUES (28,'wade','$2b$10$/ArLyw3NfyY7mrLrVcjIpe/oufR.EllcGZVrjohqkl.driRVKZcbC'),(29,'admin','$2b$10$xr7784WouBApc8Z5vYZ8kuIv2BS7MazoGNEKVD.iceZlKcmEnDk0i');
/*!40000 ALTER TABLE `registered_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resume_page_school`
--

DROP TABLE IF EXISTS `resume_page_school`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `resume_page_school` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(45) NOT NULL,
  `location` varchar(125) NOT NULL,
  `credential` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resume_page_school`
--

LOCK TABLES `resume_page_school` WRITE;
/*!40000 ALTER TABLE `resume_page_school` DISABLE KEYS */;
INSERT INTO `resume_page_school` VALUES (1,'2022 - 2023','York University - Toronto, ON','Certificate in Full-Stack Web Development'),(2,'1995 - 1997','Southern Alberta Institute of Technology - Calgary, AB','Photography Diploma'),(3,'1993 - 1994','Red River College - Winnipeg, MB','Structural Drafting Diploma'),(4,'1988 - 1992','JH Bruns Collegiate - Winnipeg, MB','High School Diploma');
/*!40000 ALTER TABLE `resume_page_school` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resume_page_work`
--

DROP TABLE IF EXISTS `resume_page_work`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `resume_page_work` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(45) NOT NULL,
  `position` varchar(100) NOT NULL,
  `workplace` varchar(100) NOT NULL,
  `experience1` varchar(255) NOT NULL,
  `experience2` varchar(255) DEFAULT NULL,
  `experience3` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resume_page_work`
--

LOCK TABLES `resume_page_work` WRITE;
/*!40000 ALTER TABLE `resume_page_work` DISABLE KEYS */;
INSERT INTO `resume_page_work` VALUES (1,'2006 - Present','Digital Print Specialist','Hemlock Printers - Vancouver, BC','I\'m the lead press and prepress technician in the Digital department at Hemlock Printers, one of the largest and most well-respected printers in North America.','Hemlock is at the cutting-edge of digital printing, serving customers such as Facebook, Airbnb and the University of British Columbia.','My technical expertise has been instrumental to the growth and success of Hemlock\'s digital department.'),(2,'2006 - Present','Freelance Photographer','Vancouver, BC','I have been a freelance photographer in Vancouver for many years. My favourite subjects are portraits, travel and outdoor photography.',NULL,NULL),(3,'2003 - 2006','Digital Print Specialist','GB Integrated Media - Winnipeg, MB','I was introduced to the exciting world of digital print and design at GB Media. I worked in the press department, running traditional offset as well as digital print equipment.',NULL,NULL),(4,'2000 - 2003','Prepress Technician','Embassy Graphics - Winnipeg, MB','Embassy Graphics is a custom prepress house that serves book and magazine publishers worldwide.','I was a member of the prepress team at Embassy, where I performed custom retouching and colour correcting of images using Adobe Photoshop.',NULL);
/*!40000 ALTER TABLE `resume_page_work` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-08-18 20:05:29
