# FS-1040 - Wade Andrew - Course Project

## Site URL: http://fs1040-wade-andrew.wade-andrew.me/

# <mark>Admin page login</mark>:
# <mark><u>User</u>: wade</mark> / <mark><u>Password</u>: password</mark>

## Logging in to the Admin page will take the user to a messages page.

## When a visitor sends a message from the Contact page, the message is added to the MySQL database, hosted on GCP. The Admin can interact with the messages on this page: view the messages, add notes to messages, delete the messages. This will update the MySQL database as well.

## Content on the Work and About pages is being pulled from the database for display. Update the database to add additional content to these pages. Use the SQL statements in "wade_database.sql" as a guide for updating database using an application such as MySQL Workbench.